const getAngkaTerbesarKedua = (dataNumbers) => {
    if (typeof dataNumbers === "object"){
        dataNumbers.sort(function(a,b){return b - a});
        return dataNumbers[1]  
    } else {
        return Error("Invalid Data Type")
    } 
}

const dataAngka = [9,4,7,7,4,3,2,2,8]
console.log(getAngkaTerbesarKedua(dataAngka))
console.log(getAngkaTerbesarKedua(0))
console.log(getAngkaTerbesarKedua())