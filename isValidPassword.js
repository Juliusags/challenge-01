const isValidPassword = (givenPassword) => {
  if(typeof givenPassword === "string"){
    if (givenPassword.length >= 8 && givenPassword.match(/[a-z]/) && 
    givenPassword.match(/[A-Z]/) && 
    givenPassword.match(/[0-9]/)){ 
      return true
    } else {
      return false
    }
  } else {
    return Error("Invalid Input")
  }
}

console.log(isValidPassword('Meong2021'))
console.log(isValidPassword('meong2021'))
console.log(isValidPassword('@eong'))
console.log(isValidPassword('Meong2'))
console.log(isValidPassword(0))
console.log(isValidPassword())